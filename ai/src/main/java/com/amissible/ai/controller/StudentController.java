package com.amissible.ai.controller;

import com.amissible.ai.model.StudentDetails;
import com.amissible.ai.services.StudentDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/quick_evaluation/")
public class StudentController {
    @Autowired
    StudentDaoImpl studentDao;

    @PostMapping("submit")
    ResponseEntity submitDetails(@RequestBody StudentDetails studentDetails) {
        studentDao.addStudent(studentDetails);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //testing purposes
    @GetMapping("get")
    Optional<StudentDetails> getDetails(@RequestParam Long serialId) {
        return studentDao.getStudent(serialId);
    }

    @GetMapping("get_all")
    List<StudentDetails> getAllStudents() {
        return studentDao.getAllStudents();
    }
}



