package com.amissible.ai.model;

import javax.persistence.*;


@Entity
@Table(name="STUDENT_DETAILS")
public class StudentDetails {
    @Id
    @Column(name = "SID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long student_id;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "EMAIL", unique = true)
    private String email;
    @Column(name = "DOB")
    private String dob;
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "QUALIFICATION")
    private String qualification;
    @Column(name = "BRANCH", nullable = true)
    private String branch;
    @Column(name = "INTERESTS")
    private String areaOfInterest;

    StudentDetails() {
    }
    
    public StudentDetails(Long student_id, String firstName,
                          String lastName, String email, String dob,
                          String gender, String qualification, String branch,
                          String areaOfInterest) {
        this.student_id = student_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.gender = gender;
        this.qualification = qualification;
        this.branch = branch;
        this.areaOfInterest = areaOfInterest;
    }

    public Long getStudent_id() {
        return student_id;
    }

    public void setStudent_id(Long student_id) {
        this.student_id = student_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAreaOfInterest() {
        return areaOfInterest;
    }

    public void setAreaOfInterest(String areaOfInterest) {
        this.areaOfInterest = areaOfInterest;
    }

    @Override
    public String toString() {
        return "StudentDetails{" +
                "student_id=" + student_id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", qualification='" + qualification + '\'' +
                ", branch='" + branch + '\'' +
                ", areaOfInterest='" + areaOfInterest + '\'' +
                '}';
    }
}
