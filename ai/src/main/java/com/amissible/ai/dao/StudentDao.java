package com.amissible.ai.dao;

import com.amissible.ai.model.StudentDetails;

import java.util.List;
import java.util.Optional;

public interface StudentDao {
    public Boolean addStudent(StudentDetails studentDetails);
    public Optional<StudentDetails> getStudent(long serialId);
    public List<StudentDetails> getAllStudents();
}
