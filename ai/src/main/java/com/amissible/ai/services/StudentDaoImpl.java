package com.amissible.ai.services;

import com.amissible.ai.dao.StudentDao;
import com.amissible.ai.model.StudentDetails;
import com.amissible.ai.repository.StudentDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentDaoImpl implements StudentDao {

    @Autowired
    StudentDetailsRepository studentDetailsRepository;

    @Override
    public Boolean addStudent(StudentDetails studentDetails) {
        studentDetailsRepository.save(studentDetails);
        return true;
    }

    @Override
    public Optional<StudentDetails> getStudent(long serialId) {
        return studentDetailsRepository.findById(serialId);
    }

    @Override
    public List<StudentDetails> getAllStudents() {
        return studentDetailsRepository.findAll();
    }
}
